
const {Builder, By, Key, until} = require('selenium-webdriver');

(async function firstScript() {
  try {
    let driver = await new Builder().forBrowser('chrome').build();

    await driver.get('http://10.0.1.192');
    driver.manage().setTimeouts({implicit: 5000});

    //let usernameField = await driver.wait(until.elementLocated(By.id('email')), 10000);
    let usernameField = await driver.findElement(By.id('email'));


    await usernameField.sendKeys('vladocsi');
    await usernameField.submit();

    await driver.quit();
  } catch (error) {
    console.log(error)
  }
})();